import pytest

def increment(x):
    return x + 1

@pytest.mark.increment
def test_increment():
    assert increment(3) == 4
    assert increment(-1) == 0
    assert increment(0) == 1
    assert increment(10) == 11