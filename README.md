# Overview
The purpose of this repo is to deploy a langchain application in AWS elastic beanstalk (EB) using gitlab-ci pipelines.

## GitLab-CI Pipeline Features.
- Includes jobs for unit testing, code quality evaluation, application local smoke test, building and manually trigger deployment, and destruction of the application.
- Leverage the environments feature in gitlab (for environment management (link and stop)).
- Generate junit reports for the unit and code quality tests that can conveniently be accessed in gitlab.

If you are new to gitlab-ci check these resources:
* [GitLab CI/CD demo](https://www.youtube-nocookie.com/embed/ljth1Q5oJoo)
* [Get started with GitLab CI](https://docs.gitlab.com/ee/ci/)


# Other README Docs
* [README_aws-beanstalk](https://gitlab.com/playground_jcl/langchain_agents_demo/-/blob/main/docs/README_aws-beanstalk.md?ref_type=heads)
* [README_agents](https://gitlab.com/playground_jcl/langchain_agents_demo/-/blob/main/docs/README_agents.md?ref_type=heads)
* [README_langchain_app](https://gitlab.com/playground_jcl/langchain_agents_demo/-/blob/main/docs/README_langchain_app.md?ref_type=heads)