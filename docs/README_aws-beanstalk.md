# Mar 15

Go to Elastic Beanstalk in aws console, on the left panel, go to the existing application, if you have no application create one first.

On the existing app clic on create environment, these environements will be use to deploy versions later. Select
* Web server environment
* Select and enviroment name, e.g. prod, staging or specific review. The gitlab-ci pipelines assume APP_ENV_NAME: "staging"
* Leave the domain name empty, this will be created a creation time.
* On platform, use python, python 3.11, and 4.0.9 for version (This is the most recent at the time this is written).
* For application code use sample application, this code will be replaced later when we update the application version.
* Use single instance (eligible for free tier). Click next.

Service role (Important)

The service roles are associated authentication and permissions to manage your EB environments and EC2.
* If you do not have a elasticbeanstalk service role select create and use new service role.
* If you already have a existing service role for elasticbeanstalk just select it.
* The EC2 key pair can be empty, we are not connecting to the logs using this option this time.
* For the EC2 instance profile, use or create a IAM role with full access to EC2. 
* Skip to review and submit.

Check the enviroment was deployed properly by accessing the domain url specified in the **Environment overview**. This should show a demo python app.

This is need to run the pipeline.

## Deployment with containers
Check some useful documentation [here](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create_deploy_docker.html)

## Post deployment
Alway that there is an app deployment a zip file is pushed to s3 and this needs to be removed


# Mar 12
* sed -i does not work well in mac, even in alpine it works weird.
* installing requirements based on local modules with pip, by default poetry will use the git repo configuration for remote and the commit to respond to pip freeze for that local package, keep that in mind when generating requirements.txt. You might need to change the type of git connection from ssh to https, and add credentials if repo is private.
* To add packages in beanstalk the .ebextensions is used, take a look at content for more details.
* To add environment variables in beanstalk the .ebextension/environment.config is used, take a look at content for more details.

# Mar 7 12:23

Check for the sample code [here](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create-deploy-python-flask.html) for instructions on how to deploy a flask app.

Create new application in portal using a template [here](https://us-east-2.console.aws.amazon.com/elasticbeanstalk/home?region=us-east-2#/create-application)

zip your app code in a bundle
```bash
zip -r archive.zip . -x "$(cat .ebignore)"
```

upload bundle zip to s3
```bash
aws s3 cp ./archive.zip s3://elasticbeanstalk-us-east-2-557732480276/archive.zip --profile eb-cli
```

create application version
```bash
aws elasticbeanstalk create-application-version --application-name langchain_agents_demo_2 --version-label 3 --source-bundle S3Bucket=elasticbeanstalk-us-east-2-557732480276,S3Key=archive.zip --region "us-east-2" --profile eb-user 
```

update app version
```bash
aws elasticbeanstalk update-environment --application-name langchain_agents_demo_2 --environment-name "Langchainagentsdemo2-env" --version-label 1 --region "us-east-2" --profile "eb-user" 
```
#########


# How to deploy to EBS
* Go to aws and login
* Select compute Elastic Beanstalk
* Create a new compute resource:
  * Configure a new environment.
    * Select web server environment as the **environment tier**.
    * **Name the application and the environment** (adding prod, staging or something like is good idea to easily identify)
    * For **platform type** select Managed platform and select python and the required version (For this repo use 3.11)
    * Select sample application as the **Application code**
    * Select single instance (free tier available) as the *presets*
    * Then Next
  * In configure service access select, create and use new service role. Do not leave blank **EC2 instance profile**.
  * Then Next.
  * Skip to review and submit. 
  This will start the deployment of the sample application (have some coffee), that will be overridden later.

How to run FASTAPI app in Beanstalk guide [here](https://gist.github.com/malthunayan/dd9923e88bf1b4d29c78d5f4d0141518).
* Install the aws CLI [REF](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html).
```bash
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
```

# Revisiting Elastic Beanstalk

Once in the elastic beanstalk >> create application
Configure environment >>
* Select web server environment  
* set application name 
* Set the environment name
* Leave domain blank if do not interested on fixing it
* Select managed platform >> Platform (Python or Docker)
* Keep Sample application
* For Presets select single instance (For free tier eligible)
* click NEXT

Configure service access
* Create and use new service role if needed. (I am using aws-elasticbeanstalk-service-role)
* Select the EC2 instance profile (elasticbeanstalk-ec2-role). Otherwise you will get an error like "Environment must have instance profile associated with it."

* TODO: (Check uses of the EC2 key pair and instance profile)
* click NEXT
* Skip to Review

Check [here](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create-deploy-python-flask.html) for instructions on how to deploy a flask app.

# EB deployment using EB CLI (An alternative to the AWS CLI)
* install the aws eb cli `pip install -U --user awsebcli`

## As a Python app (Following tutorial (here)[https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create-deploy-python-flask.html])


* create credentials and grant permision policy
- go to IAM select your user
- Add the AdministratorAccess-AWSElasticBeanstalk policy by cliking add permisions >> attache policies directly and searching for the policy
- Create credentials in **security credentials** tab access keys section, (select CLI use case), and save them in your ~/.aws/config file, it shold look like
```
[profile eb-cli]
aws_access_key_id = XXXXXX
aws_secret_access_key = XXXXXXXXX
```

* Initialize an EB CLI repo 
```bash
eb init -p python-3.7 flask-tutorial --region us-east-2 --profile eb-cli
```

* Make sure to include any unnecesary files in the .ebignore file, for example venvs.
* Optionally a key pair can be configure to be able to connect throuhg ssh to the deployed instance (check [here](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create-deploy-python-flask.html) for more details)

* create an environment. This will take care of ziping project and uploading it to S3 as well as releasing the version into EBS
```
eb create flask-env
```
* To redeploy after creation use
```
eb deploy
```

# EB deployment using AWS CLI
## TODO: COMPLETE HERE
```bash
pip install -U awscli
```

create application
```bash
aws elasticbeanstalk create-application --application-name langchain_agents_demo --region "us-east-2" --profile eb-cli --description "101 app description here"
```

prerrequisite: create the credentials
In IAM add user gitlab-ci, gurantee it has programmatic, permission give access to only S3 (an existing policy) (Amazon S3 Full Access) then create user.
Create access key (use case CLI) Copy key and Secret and Access key
save it to a profile in the ~/.aws/config file

#####
create a environment:
select:
- type of environment web server vs worker
- app name
- env name
- env domain

#####

To deploy the EBS.
Need to create app **version** and update **the environment**. 
**Important note:** This requires an app-instance profile, creating it using aws cli gets tricky.

zip your app code in a bundle
```bash
zip -r archive.zip . -x "$(cat .ebignore)"
```

upload bundle zip to s3
```bash
aws s3 cp ./archive.zip s3://elasticbeanstalk-us-east-2-557732480276/archive.zip --profile eb-cli
```

```bash
aws elasticbeanstalk create-application --application-name langchain_agents_demo --region "us-east-2" --profile "eb-user"
```

create application version
```bash
aws elasticbeanstalk create-application-version --application-name langchain_agents_demo --version-label 1 --source-bundle S3Bucket=elasticbeanstalk-us-east-2-557732480276,S3Key=archive.zip --region "us-east-2" --profile eb-user 
```

create environment. 
```bash
aws elasticbeanstalk create-environment --application-name langchain_agents_demo --environment-name "production" --cname-prefix langchain-agents-url --version-label 1 --region "us-east-2" --profile "eb-user" --solution-stack-name "64bit Amazon Linux 2023 v4.0.9 running Python 3.11" --tier Name=WebServer,Type=Standard --instance-profile aws-elasticbeanstalk-ec2-role
# preset Single instance (Free tier eligible)
```

```bash
aws elasticbeanstalk update-environment --application-name langchain_agents_demo --environment-name "production" --version-label 1 --region "us-east-2" --profile "eb-user" 
```